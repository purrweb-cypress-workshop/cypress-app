const { defineConfig } = require("cypress");
const cypressEnv = require("./cypress.env.json");

module.exports = defineConfig({
  e2e: {
    excludeSpecPattern: "cypress/e2e/examples",
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    env: cypressEnv,
  },
});
