describe("Purrweb Google search", () => {
  it("Should be first", () => {
    cy.visit("https://www.google.com/");
    cy.get(".gLFyf").clear();
    cy.get(".gLFyf").type("purrweb");
    cy.get(".CqAVzb > center > .gNO89b").click();
    cy.url().should("contains", "https://www.google.com/search");

    cy.get(".g")
      .first()
      .should("contain.text", "Purrweb")
      .should("contain.text", "https://www.purrweb.com");
  });
});
