Cypress.on("uncaught:exception", (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});

describe("portfolio page", () => {
  beforeEach(() => {
    cy.visit("https://www.purrweb.com/portfolio/");
  });

  it("display all categories", () => {
    cy.get(".portfolio-head__filter-item").should("have.length", 15);
    cy.get(".portfolio-head__filter-item").first().contains("Healthcare");
    cy.get(".portfolio-head__filter-item").eq(0).contains("Healthcare");
  });

  describe("grecha project", () => {
    it("Default filters. Display grecha project", () => {
      cy.get(".portfolio-card--grecha").should("be.visible");
    });

    it("Foodtech category filter. Display grecha project", () => {
      cy.get(".portfolio-head__filter-item").contains("Foodtech").click();
      cy.get(".portfolio-card--grecha").should("be.visible");
    });

    it("Healthcare category filter. Not display grecha project", () => {
      cy.get(".portfolio-head__filter-item").contains("Healthcare").click();
      cy.get(".portfolio-card--grecha").should("not.be.visible");
    });
  });
});
