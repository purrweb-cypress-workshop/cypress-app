describe("My First Test", () => {
  it("Does much!", () => {
    expect(true).to.equal(true);
  });

  it.skip("Does not do much!", () => {
    expect(true).to.equal(false);
  });

  it("Visit google.com", () => {
    cy.visit("https://www.google.com/");
    cy.url().should("contains", "https://ogs.google.com/u/0/widget/app");
    cy.get(".gLFyf").type("hhh");
    cy.url().should("contains", "https://www.google.com/search");
  });

  describe("Type text", () => {
    it("Search result page should be opened ", () => {
      /* ==== Generated with Cypress Recorder ==== */
      cy.visit("https://www.google.com/");
      cy.get(".gLFyf").clear();
      cy.get(".gLFyf").type("purrweb");
      cy.get(".CqAVzb > center > .gNO89b").click();
      /* ==== End Cypress Recorder ==== */
      cy.url().should("contains", "https://www.google.com/search");
    });
  });
});
