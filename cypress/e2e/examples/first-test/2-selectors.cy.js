describe("Get elements", () => {
  it("By tag", () => {
    cy.visit("https://www.google.com/");
    cy.get("img");
  });

  it("By class", () => {
    cy.visit("https://www.google.com/");
    cy.get(".gNO89b");
  });

  it("By id", () => {
    cy.visit("https://about.google/");
    cy.get("#module-anchor-statement-mission-statement");
  });

  it("By attribute", () => {
    cy.visit("https://www.google.com/");
    cy.get('[type="Submit"]');
  });
});

describe("Type text", () => {
  it("Search result page should be opened ", () => {
    /* ==== Generated with Cypress Recorder ==== */
    cy.visit("https://www.google.com/");
    cy.get(".gLFyf").clear();
    cy.get(".gLFyf").type("purrweb");
    cy.get(".CqAVzb > center > .gNO89b").click();
    /* ==== End Cypress Recorder ==== */
    cy.url().should("contains", "https://www.google.com/search");
  });
});
