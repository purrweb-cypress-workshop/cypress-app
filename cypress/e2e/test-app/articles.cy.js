describe("with authorization", () => {
  describe("load articles", () => {
    before(() => {
      cy.login(Cypress.env().adminEmail, Cypress.env().adminPassword);
    });

    it("should articles loaded", () => {
      cy.visit("localhost:3009/articles");
      cy.get("h1").contains("Articles");
      cy.get(".article-block").should("have.length.above", 1);
    });
  });

  describe("mock articles", () => {
    before(() => {
      cy.login(Cypress.env().adminEmail, Cypress.env().adminPassword);
      cy.intercept("http://localhost:3010/666/articles", {
        fixture: "articles.json",
      });
    });
    it("should articles show", () => {
      cy.visit("localhost:3009/articles");
      cy.get(".article-block").should("have.length", 3);
    });
  });

  describe("articles-page", () => {
    beforeEach(() => {
      cy.intercept("http://localhost:3010/666/articles", {
        fixture: "articles.json",
      });
      cy.login(Cypress.env().adminEmail, Cypress.env().adminPassword);
    });
    afterEach(() => {
      cy.logout();
    });
    it("snapshot", () => {
      cy.visit("localhost:3009/articles");
      cy.percySnapshot("articles-page");
    });
  });

  describe("article-block element", () => {
    beforeEach(() => {
      cy.intercept("http://localhost:3010/666/articles", {
        fixture: "article.json",
      });
      cy.login(Cypress.env().adminEmail, Cypress.env().adminPassword);
    });
    afterEach(() => {
      cy.logout();
    });
    it("snapshot", () => {
      cy.visit("localhost:3009/articles");
      cy.percySnapshot("article-block");
    });
  });

  context("iphone-8 resolution", () => {
    beforeEach(() => {
      cy.viewport("iphone-8");
      cy.intercept("http://localhost:3010/666/articles", {
        fixture: "article.json",
      });
      cy.login(Cypress.env().adminEmail, Cypress.env().adminPassword);
    });
    it("snapshot", () => {
      cy.visit("localhost:3009/articles");
      cy.get(".article-block").percySnapshotElement(
        "article-block-element iphone-8 resolution"
      );
    });
  });
});
