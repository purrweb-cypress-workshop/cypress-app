describe("homepage", () => {
  before(() => {
    cy.visit("localhost:3009");
  });
  after(() => {});

  describe("without authorization", () => {
    it("Contains header", () => {
      cy.get(".Header");
    });

    it("Contains Home page text", () => {
      cy.get("h3").contains("Home page");
    });

    it("Contains login button", () => {
      cy.get("button").contains("login");
    });

    it("Contains articles link ", () => {
      cy.get(".Articles-link")
        .contains("Articles")
        .should("have.attr", "href", "/articles");
    });
  });

  describe("with authorization", () => {
    beforeEach(() => {
      cy.login(Cypress.env().adminEmail, Cypress.env().adminPassword);
    });

    afterEach(() => {
      cy.logout();
    });

    it("Contains logout button", () => {
      cy.get(".AccountInfo").get("button").contains("logout");
    });
  });
});
