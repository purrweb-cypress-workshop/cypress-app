// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add("login", (email, password) => {
  cy.visit("http://localhost:3009/login");
  cy.get('[type="text"]').type(email);
  cy.get('[type="password"]').clear();
  cy.get('[type="password"]').type(password);
  cy.get("form > div > button").click();
  cy.url().should("eq", "http://localhost:3009/");
});

Cypress.Commands.add("logout", () => {
  cy.visit("localhost:3009/logout");
});

Cypress.Commands.add(
  "percySnapshotElement",
  { prevSubject: true },
  (subject, name, options) => {
    cy.percySnapshot(name, {
      domTransformation: (documentClone) =>
        scope(documentClone, subject.selector),
      ...options,
    });
  }
);
function scope(documentClone, selector) {
  const element = documentClone.querySelector(selector);
  documentClone.querySelector("body").innerHTML = element.outerHTML;
  return documentClone;
}
